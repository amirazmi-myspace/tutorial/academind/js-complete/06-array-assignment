const numbers = [1, 3, 5, 7, 23, 6, 7, 34, 6, 4];

const numsGreaterFive = numbers.filter((number) => number > 5);
const numObj = numbers.map((num, index) => ({ num, index }))
const mulNum = numbers.reduce((prevNum, currNum) => prevNum * currNum, 1);

console.log('> 5', numsGreaterFive);
console.log('num object', numObj);
console.log('num multiply', mulNum);

const findMax = (...allNumbers) => {
    const max = Math.max(...allNumbers);
    const min = Math.min(...allNumbers);
    return {max, min}
};
console.log(findMax(1, 23, 56, 7));

const userIds = new Set();
userIds.add(10);
userIds.add(4);
userIds.add(10);

console.log(userIds);